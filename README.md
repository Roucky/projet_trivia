# **Projet de sprint trivia**

Nous avons créée des branches pour chaques sprints : 

Branche numéro 1 : **Tour_de_chauffe** : [Voir les commits](https://gitlab.com/Roucky/projet_trivia/-/network/Tour_de_chauffe?utf8=✓&extended_sha1=&filter_ref=1)

Branche numéro 2 : **Premier_Sprint** : [Voir les commits](https://gitlab.com/Roucky/projet_trivia/-/network/Premier_Sprint?utf8=✓&extended_sha1=&filter_ref=1)

Branche numéro 3 : **Deuxieme_Sprint** : [Voir les commits](https://gitlab.com/Roucky/projet_trivia/-/network/Deuxieme_Sprint?utf8=✓&extended_sha1=&filter_ref=1)

Branche numéro 4 : **Troisieme_Sprint** : [Voir les commits](https://gitlab.com/Roucky/projet_trivia/-/network/Quartrieme_Sprint?utf8=✓&extended_sha1=&filter_ref=1) Suite a un problème de push la branche "Troisieme_Sprint" a été push sur la branche "Quatrieme_Sprint" (Le troisieme sprint va du commit : "fix conflict" du Feb 24, 2021 9:34am GMT+0100 (code SHA : a06e49bdc8dd9f1e3db1d6e8811fe70ecba64954) au commit "FEAT Adding Leaderbord" du Feb 24, 2021 11:57am GMT+0100 (code SHA : 067d0faec2559c7b610dc623719988b1e682bcff))

Branche numéro 5 : **Quatrieme_Sprint** : [Voir les commits](https://gitlab.com/Roucky/projet_trivia/-/network/Quartrieme_Sprint?utf8=✓&extended_sha1=&filter_ref=1) A partir du commit "FEAT Adding Leaderbord" du Feb 24, 2021 11:57am GMT+0100 (code SHA : 067d0faec2559c7b610dc623719988b1e682bcff)

Branche numéro 6 : **Cinquieme_Sprint** : [Voir les commits](https://gitlab.com/Roucky/projet_trivia/-/network/Cinquieme_Sprint?utf8=✓&extended_sha1=&filter_ref=1)

Branche numéro 7 : **Sixieme_Sprint** : [Voir les commits](https://gitlab.com/Roucky/projet_trivia/-/network/Sixieme_Sprint?utf8=✓&extended_sha1=&filter_ref=1)

**CONCLUSION**

3 choses apprises :
- Organisation de l'équipe
- Pair programming
- Travail par sprint avec débrief

3 choses à changer dans le code :
- Refactoring du code existant
- Architecture
- 

3 choses à changer dans l'organisation :
- Essayer de changer les rôles
- Davantage d'échanges entre les membres
- Privilégier davatange le pair programming





